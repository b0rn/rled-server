import board
import neopixel

class LEDController:
    def __init__(self,led_count,brightness):
        self.strip = neopixel.NeoPixel(board.D18,led_count,brightness=1.0,auto_write=False)
        self.brightness = brightness
        self.reload(brightness)

    def reload(self,brightness):
        self.brightness = brightness
        self.fill((0,0,0))
        self.strip.show()

    def exit(self):
        self.strip.deinit()

    def numPixels(self):
        if self.strip is not None:
            return self.strip.n
        return 1

    def setPixelColor(self,i,color):
        if self.strip is not None:
            self.strip[i] = [c * self.brightness for c in color]

    def fill(self,color):
        if self.strip is not None:
            self.strip.fill(color)

    def show(self):
        if self.strip is not None:
            self.strip.show()
