#!/usr/bin/python3

import socket
import json

from config import Config
from ledcontroller import LEDController
from apps.audiovisualizer import AudioVisualizer
from apps.lighting import Lighting

class Server():
    def __init__(self):
        self.config = Config()
        self.ledController = LEDController(self.config.LED_COUNT, self.config.LED_BRIGHTNESS)
        self.ledApp = None
        self.relaunchApp = None
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.running = False
        self.clientSocket = None

    def run(self):
        self.socket.bind((self.config.HOST,self.config.PRIMARY_PORT))
        self.socket.listen(1)
        self.running = True
        print("Server listening on %s:%d" % (self.config.HOST,self.config.PRIMARY_PORT))

        try:
            while self.running:
                self.clientSocket, address = self.socket.accept()
                while self.running:
                    recv = self.clientSocket.recv(1024)
                    if not recv:
                        break
                    self.handle(recv)
                self.clientSocket.close()
                self.clientSocket = None
        except:
            pass
        print("Server stopped")
        print("Bye :)")

    def handle(self,data):
        try:
            #Deserialize json data
            payload = json.loads(data)

            if payload["cmd"] == "getconfig":
                self.clientSocket.send(bytes(json.dumps(self.config.getSettings()),"utf-8"))
            elif payload["cmd"] == "setconfig":
                try:
                    if self.ledApp is not None:
                        self.ledApp.terminate()
                    self.config.parseJSON(payload["settings"])
                    self.ledController.reload(self.config.LED_BRIGHTNESS)
                    self.clientSocket.send(bytes(json.dumps({"SET_CONFIG_RESP":True}),"utf-8"))
                    if self.ledApp is not None:
                        self.ledApp = self.relaunchApp()
                        self.ledApp.start()
                except Exception as e:
                    print(e)
                    self.clientSocket.send(bytes(json.dumps({"SET_CONFIG_RESP":str(e)}),"utf-8"))
            elif payload["cmd"] == "stop":
                self.ledApp.terminate()
                self.ledApp = None
                print("App terminated")
            elif payload["cmd"] == "audio":
                sample_rate = payload["SAMPLE_RATE"]
                sample_max = payload["SAMPLE_MAX"]
                self.relaunchApp = lambda : AudioVisualizer(self.ledController, self.config.HOST, self.config.SECONDARY_PORT,sample_rate,sample_max)
                app = self.relaunchApp()
                print("Launching audio")
                self.launchLEDApp(app)
            elif payload["cmd"] == "lighting":
                #self.relaunchApp = lambda : Lighting(self.ledController, self.config.HOST, self.config.SECONDARY_PORT)
                print(payload)
                self.relaunchApp = lambda : Lighting(self.ledController,payload["mode"],payload["colors"])
                app = self.relaunchApp()
                print("Launching lighting")
                self.launchLEDApp(app)
            elif payload["cmd"] == "wipe":
                pass
            elif payload["cmd"] == "chase":
                pass
            elif payload["cmd"] == "rainbow":
                pass
            elif payload["cmd"] == "rainbowCycle":
                pass
            elif payload["cmd"] == "chaseRainbow":
                pass
        except Exception as e:
            print(e)

    def launchLEDApp(self,app):
        payload = {}
        try:
            if self.ledApp is not None:
                self.ledApp.terminate()
            self.ledApp = app
            self.ledApp.start()
            payload["APP_LAUNCHED"] = True
        except Exception as e:
            print(e)
            payload["APP_LAUNCHED"] = e
        finally:
            self.clientSocket.send(bytes(json.dumps(payload),"utf-8"))

    def terminate(self):
        if self.ledApp is not None:
            self.ledApp.terminate()
        self.running = False
        if self.clientSocket is not None:
            self.clientSocket.close()
        self.socket.close()
        self.ledController.exit()

if __name__ == "__main__":
    import signal
    import sys

    server = Server()

    def signal_handler(sig, frame):
        try:
            server.terminate()
            sys.exit(0)
        except:
            pass

    signal.signal(signal.SIGINT, signal_handler)
    server.run()
