import json

class Config:
    def __init__(self):
        self.__DICT = None
        try:
            with open("config.json","r") as data:
                self.parseJSON(data,file=True)
        except:
            pass

    def parseJSON(self,data,file=False):
        config = json.load(data) if file else data
        tmp = {}

        tmp["HOST"] = config["HOST"]
        tmp["PRIMARY_PORT"] = config["PRIMARY_PORT"]
        tmp["SECONDARY_PORT"] = config["SECONDARY_PORT"]
        tmp["LED_COUNT"] = config["LED_COUNT"]
        tmp["LED_BRIGHTNESS"] = config["LED_BRIGHTNESS"]

        for key,value in tmp.items():
            setattr(self,key,value)
        self.__DICT = tmp

        if "SAVE" in config and config["SAVE"] == True:
            with open("config.json","w") as file:
                json.dump(tmp,file)

    def getSettings(self):
        if self.__DICT is not None:
            return self.__DICT
        return {}
