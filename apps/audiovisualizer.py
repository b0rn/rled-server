import socket
import numpy as np
import pickle
from threading import Thread

from .ledapp import LEDApp

class AudioVisualizer(LEDApp,Thread):
    def __init__(self,ledController,host,port,sample_rate=44100,sample_max=100000):
        Thread.__init__(self)
        self.ledController = ledController
        self.host = host
        self.port = port
        self.sample_rate = sample_rate
        self.sample_max = sample_max
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.setblocking(0)

    def run(self):
        self.socket.bind((self.host, self.port))
        self.running = True
        udpPacketMaxSize = 65507

        while self.running:
            try:
                data = self.socket.recv(udpPacketMaxSize)
            except:
                continue
            try:
                payload = pickle.loads(data)
                for i in range(len(payload)):
                    self.ledController.setPixelColor(i,tuple(payload[i]))
                self.ledController.show()
            except Exception as e:
                print(e)

    def launch(self):
        self.start()

    def terminate(self):
        self.running = False
        self.socket.close()
        self.ledController.fill((0,0,0))
