import socket
import numpy as np
import pickle
from threading import Thread

from .ledapp import LEDApp

class Lighting(LEDApp):
    def __init__(self,ledController,mode,colors):
        self.ledController = ledController
        self.mode = mode
        self.colors = colors

    def start(self):
        if self.mode == "uni":
            self.ledController.fill(self.colors)
        else:
            for i in range(len(self.colors)):
                self.ledController.setPixelColor(i,self.colors[i])
        self.ledController.show()

    def terminate(self):
        self.ledController.fill(0)
        self.ledController.show()
